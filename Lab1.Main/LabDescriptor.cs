﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(Contract.IMałpa);
        
        public static Type ISub1 = typeof(Contract.IGoryl);
        public static Type Impl1 = typeof(Implementation.Goryl);
        
        public static Type ISub2 = typeof(Contract.IPawian);
        public static Type Impl2 = typeof(Implementation.Pawian);
        
        
        public static string baseMethod = "Metoda1";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Metoda2";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Metoda3";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Nowa";
        public static string collectionConsumerMethod = "Dalej";

        #endregion

        #region P3

        public static Type IOther = typeof(Contract.IOlbrzym);
        public static Type Impl3 = typeof(Implementation.Olbrzym);

        public static string otherCommonMethod = "Metoda1";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
