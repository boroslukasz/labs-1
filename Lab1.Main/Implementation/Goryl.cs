﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1.Implementation
{
    public class Goryl: Contract.IGoryl
    {
        public Goryl() { }

        public string Metoda1()
        {
            return "Metoda1 Goryl";
        }

        public string Metoda2()
        {
            return "Metoda2 Goryl";
        }
    }
}
